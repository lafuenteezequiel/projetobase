<?php
/**
 * Created by PhpStorm.
 * User: Ezequiel Lafuente
 * Date: 23/11/2018
 * Time: 16:15
 */

namespace App\Repositories;

use App\Repositories\Contracts\ClientRepositoryInterface;
use App\Models\Client;

class ClientRepository implements ClientRepositoryInterface
{
    private $model;

    public function __construct(Client $model)
    {
        $this->model = $model;
    }

    public function findAll()
    {
        return $this->model->all();
    }
}