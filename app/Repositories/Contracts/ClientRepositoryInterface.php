<?php

/**
 * Created by PhpStorm.
 * User: Ezequiel Lafuente
 * Date: 23/11/2018
 * Time: 16:15
 */

namespace App\Repositories\Contracts;

interface ClientRepositoryInterface
{
    public function findAll();
}