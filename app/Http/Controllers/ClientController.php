<?php

/**
 * Created by PhpStorm.
 * User: Ezequiel Lafuente
 * Date: 23/11/2018
 * Time: 16:15
 */

namespace App\Http\Controllers;

use App\Repositories\Contracts\ClientRepositoryInterface;

class ClientController extends Controller
{

    protected $clientRepository;

    public function __construct(ClientRepositoryInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }


    public function listar()
    {
        return $this->clientRepository->findAll();
    }
}
