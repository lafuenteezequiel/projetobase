<?php

/**
 * Created by PhpStorm.
 * User: Ezequiel Lafuente
 * Date: 23/11/2018
 * Time: 16:15
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $table = 'clientes';

    protected $fillable = [
        'id',
        'nome',
        'email'
    ];
}
